[TOC]

官方文档：https://webpack.js.org/guides/getting-started/

# 安装
```
//latest release
npm install --save-dev webpack
//specific version
npm install --save-dev webpack@v3.4.1
```

# Setup
```
npm init -y
npm install --save-dev webpack
```

如果页面只引入一个js, 则补充完整项目结构如下：
```shell
.
|____dist
| |____bundle.js
| |____index.html
|____package.json
|____src
| |____index.js
|____webpack.config.js
```

但，通常页面会有多个js:
```
.
|____dist
| |____app.bundle.js
| |____index.html
| |____print.bundle.js
|____package.json
|____src
| |____index.js
| |____print.js
|____structure.txt
|____webpack.config.js

```


## 自动清理dist目录
```
npm install clean-webpack-plugin --save-dev

//webpack.config.js
const CleanWebpackPlugin = require('clean-webpack-plugin');

...
plugins: [
new CleanWebpackPlugin(['dist']),
new HtmlWebpackPlugin({
  title: 'Output Management'
})
],

```
## 自动生成index.html
如何修改了js的名字，会产生一个问题，html中引用的地方也要改。这里有个插件可以自动生成一个全新的html模板。
```
npm install --save-dev html-webpack-plugin

//webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin');

....
plugins: [
 new HtmlWebpackPlugin({
   title: 'Output Management'
 })
]
```


## package.json
```json
{
  "name": "webpack-demo",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "build": "webpack",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "clean-webpack-plugin": "^0.1.16",
    "html-webpack-plugin": "^2.29.0",
    "webpack": "^3.4.1"
  },
  "dependencies": {
    "lodash": "^4.17.4"
  }
}

```

## webpack.config.js
```js
var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.js',
    print: './src/print.js'
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Output Management'
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};
```

# 运行
```
npm run build
```
这时候，会在dist目录下生成js和index.html.



# 开发模式
打包后的js不便调试，可以采用开发模式：
webpack.config.js:
```diff
const path = require('path');
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  const CleanWebpackPlugin = require('clean-webpack-plugin');

  module.exports = {
    entry: {
      app: './src/index.js',
      print: './src/print.js'
    },
+   devtool: 'inline-source-map',
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new HtmlWebpackPlugin({
        title: 'Development'
      })
    ],
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
};
```

## 热部署
js修改后想要立即生效：
```
npm install --save-dev webpack-dev-server
```
然后在webpack.config.js:
```diff
const path = require('path');
  const HtmlWebpackPlugin = require('html-webpack-plugin');

  module.exports = {
    entry: {
      app: './src/index.js',
      print: './src/print.js'
    },
    devtool: 'inline-source-map',
+   devServer: {
+     contentBase: './dist'
+   },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new HtmlWebpackPlugin({
        title: 'Development'
      })
    ],
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    }
  };
```
接下来，配置npm的package.json:
```diff
{
    "name": "development",
    "version": "1.0.0",
    "description": "",
    "main": "webpack.config.js",
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1",
      "watch": "webpack --progress --watch",
+     "start": "webpack-dev-server --open"
      "build": "webpack"
    },
    "keywords": [],
    "author": "",
    "license": "ISC",
    "devDependencies": {
      "css-loader": "^0.28.4",
      "csv-loader": "^2.1.1",
      "file-loader": "^0.11.2",
      "html-webpack-plugin": "^2.29.0",
      "style-loader": "^0.18.2",
      "webpack": "^3.0.0",
      "xml-loader": "^1.2.1"
    }
}
```

运行`npm start`可以启动一个静态服务器，每次修改js后都会自动编译。
